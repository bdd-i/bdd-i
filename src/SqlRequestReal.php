<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI;

use Luri\BddI\Common\SqlRequest;

class SqlRequestReal implements SqlRequest {
	/**
	 * True if SQL request can countain named parameter (:XXX).
	 * False if SQL request must countain only anonymour parameter (?)
	 *
	 * @var bool
	 */
	protected $returnNamedParameter = false;

	/**
	 * SQL request
	 * @var array
	 */
	protected $request = [];

	/**
	 * Datas
	 * @var mixed
	 */
	protected $datas = [];


	/**
	 * Create a new SQLRequest
	 * @param mixed $sql See modifyReq() for syntax
	 */
	public function __construct($sql) {
		$this->modifyReq($sql);
	}

	/**
	 * {@inheritdoc}
	 */
	public function setNamedParameter(bool $val) {
		$this->returnNamedParameter = (bool) $val;
	}


	/**
	 * {@inheritdoc}
	 */
	public function modifyReq($sql) : SqlRequest {
		//
		// Take the SQL request
		//
		if (!empty($sql['SQL'])) {
			$req = $sql['SQL'];
		} else {
			$req = $sql;
		}
		if (!is_string($req)) {
			throw new \InvalidArgumentException("ModifReq : Param $sql is not valid.");
		}

		//
		// Extract Parameter and stock request into an array
		//

		//No space before SQL command start
		$req = ltrim ($req);
		//Process question marrk escape
		$req = str_replace('\?', '?', $req);

		$this->request=explode('?', preg_replace_callback( '~(\?|:[\w]+)~', [$this, 'extractNamedPlaceholders'], $req));

		//
		// Data process
		//
		if (!empty($sql['DATAS'])) {
			$this->addDatas($sql['DATAS']);
		}

		return $this;

	}

	/**
	 * Extract anonymous and named parameter of a SQL request
	 *
	 * Callback function for preg_replace_callback()
	 *
	 * @param array $matches
	 * @return string
	 */
	protected function extractNamedPlaceholders($matches) {
	   //On stoque l'ordre des marqueurs (annonyme et nommés)
	   if ($matches[0] != '?') {
		   $this->datas[$matches[0]] = null;
	   } else {
		   $this->datas[] = null;
	   }
	   return '?';
	}

	/**
	 * {@inheritdoc}
	 */
	public function addDatas($datas): SqlRequest {
		//If not an array, create this array
		if (!is_array($datas)) {
			//Search the first free place
			$key = array_search(null, $this->datas, true);
			if ($key===false or $key===null) {
				//Error
				throw new \OverflowException("Datas is full, please precise key for replace");
			}

			//Manualy create  the array
			$datas = [ $key => $datas];
		}

		//Transform and add
		foreach ($datas as $k => $v) {
			if (!array_key_exists($k, $this->datas)) {
				throw new \OutOfBoundsException("Field $k not found");
			}

			if (is_bool($v)) {
				$v = ($v) ? 1 : 0;
			}

			$this->datas[$k] = $v;
		}

		return $this;
	}


	/**
	 * {@inheritdoc}
	 */
	public function __toString() {
		$ch = "";
		$keys = array_keys($this->datas);

		foreach ($this->request as $k => $v) {
			//Take the string
			$ch .= $v;

			//Which glue need to add ?
			if (isset($keys[$k])) {

				if (is_array($this->datas[$keys[$k]])) {
					//Data is an array
					if (!is_int($keys[$k]) AND $this->returnNamedParameter) {
						//Need to add :XX1, :XX2...
						for ($i=1; $i<=count($this->datas[$keys[$k]]); ++$i) {
							$ch .= $keys[$k] . $i . ',';
						}
						$ch = substr($ch, 0, -1);

					} else {
						//Need to add ?
						$ch .= implode(',', array_fill(1, count($this->datas[$keys[$k]]), '?'));
					}

				} else {
					//Data is a standard value
					$ch .= (!is_int($keys[$k]) AND $this->returnNamedParameter) ? $keys[$k] : '?';
				}
			}
		}

		//Le caractère ? représente les ? echappé qu'on réintroduit sans l'échappement cette fois-ci
		return str_replace('?', '?', $ch);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getDatas() {
		$returnkey = 0;
		foreach ($this->datas as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if (!is_int($k) AND $this->returnNamedParameter) {
						//Must return :XX1, :XX2 for key
						yield $k . ($k2+1) => $v2;
					} else {
						//Must return int for key
						//post incrementation for $returnkey
						yield $returnkey++ => $v2;
					}
				}
			} else {

				if (!is_int($k) AND $this->returnNamedParameter) {
					//Use $k for key
					yield $k => $v;
				} else {
					//Must return int for key
					//post incrementation for $returnkey
					yield $returnkey++ => $v;
				}
			}
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function isData() : bool {
		return !empty($this->datas);
	}
}
?>