<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI;

use Luri\BddI\Common\Db;

class DbFactory extends DbConfig {
	/**
	 * Instance of opened SQL server
	 * @var Db[]
	 */
	protected static $servers = [];


	/**
	 * Get a Db object that represent SQL server
	 *
	 * @param string $db database ID
	 * @return \Luri\BddI\Common\Db
	 * @throws \OutOfBoundsException id $db not exist
	 */
	public static function getDbInstance($db = parent::DEFAULTDATABASE) {
		//Verify if $db exist
		if (!array_key_exists($db, parent::DBCONFIG)) {
			throw new \OutOfBoundsException("$db is not configured");
		}
		$serverid = parent::DBCONFIG[$db]['server'];

		if(!array_key_exists($serverid, self::$servers)) {
			//Create a new server connexion
			$driver = parent::SERVERCONFIG[$serverid]['lowLevelDriver'];

			self::$servers[$serverid] = new $driver(
				parent::SERVERCONFIG[$serverid]['address'],
				parent::SERVERCONFIG[$serverid]['login'],
				parent::SERVERCONFIG[$serverid]['password'],
				parent::SERVERCONFIG[$serverid]['charset']
			);
		}

		//Charge bd
		self::$servers[$serverid]->changeDb($db, parent::DBCONFIG[$db]['name']);

		return self::$servers[$serverid];
	}

	/**
	 * You MUST use DbFactory::getDbInstance()
	 * @acces private
	 */
	protected function __construct() {

	}

	/**
	 * Return default database ID
	 * @return string
	 */
	public static function getIdDatabaseDefault() {
		return parent::DEFAULTDATABASE;
	}
}
?>