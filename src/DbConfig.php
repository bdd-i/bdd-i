<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 * PLEASE NOT EDIT THIS FILE!!!!
 * This file MUST BE copy on your param file/dir and this code MUST BE included
 * before any other code.
 */
namespace Luri\BddI {
	/**
	 * Configuration Class
	 */
	abstract class DbConfig {

		/**
		 * Config of database
		 */
		protected const DBCONFIG = [
			'default' => [ //db id for use in you apps
				'name' => 'test', //Db name in SQL server
				'server' => 'local' //SQL server Id in SERVERCONFIG
			],
		];

		/**
		 * Config of server
		 */
		protected const SERVERCONFIG = [
			'local' => [ ////SQL server Id
				'login' => 'root', //Login for this connexion (you MUST not use root)
				'password' => '', //Password for this connexion (you MUST create a robust password)
				'address' => '127.0.0.1', //IP address for SQL server
				'lowLevelDriver' => '\Luri\BddI\LowLevel\LlMock', //Name of low level driver class to use
				'charset' => 'latin1' //Charset to use
			],
		];


		/**
		 * Default database
		 * @var string
		 */
		protected const DEFAULTDATABASE = 'default';


	}
}
namespace {
	// you're global code

}
?>