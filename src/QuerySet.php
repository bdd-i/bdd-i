<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI;

/**
 * Represent a set of query (in same database) who use Transaction
 *
 * (mùais ça n'invente pas le système si le driver de bas niveau utilmisé ne supporte pas les transactions
 */
class QuerySet implements \ArrayAccess, \Countable, \IteratorAggregate {
	/**
	 * Set of query to execute
	 * @var Query[]
	 */
	protected $querys = [];

	/**
	 * Reprensent the low level interface to SQL server
	 *
	 * @var Db
	 */
	protected $db = null;

	/**
	 * Name of DbId to use
	 *
	 * @var string
	 */
	protected $dbId = "";


	/**
	 * Create a new set of SQL Query
	 *
	 * If a SQL request is provided. This request is accessible via key 0 ([0])
	 *
	 * All Param is optional
	 *
	 * @param string|array $sql SQL query, see Query::addReq for syntax
	 * @param int|bool|string|array $datas you're datas, see Query::addDatas for syntax
	 * @param string $databaseId Database id where req is executed. If not provided, it's take ddefault database
	 * @return \Luri\BddI\QuerySet FluentInterface
	 */
	public static function factory($sql="", $datas = "", $databaseId="") {
		return new QuerySet($sql, $datas, $databaseId);
	}


	/**
	 * Create a new set of SQL Query
	 *
	 * If a SQL request is provided. This request is accessible via key 0 ([0])
	 *
	 * All Param is optional
	 *
	 * @param string|array $sql SQL query, see Query::addReq for syntax
	 * @param int|bool|string|array $datas you're datas, see Query::addDatas for syntax
	 * @param string $databaseId Database id where req is executed. If not provided, it's take ddefault database
	 */
	public function __construct($sql = "", $datas = "", $databaseId = "") {
		//Db connexion
		if (empty($databaseId)) {
			$databaseId = DbFactory::getIdDatabaseDefault();
		}
		$this->db = DbFactory::getDbInstance($databaseId);
		$this->dbId = $databaseId;

		//Add SQL
		if (!empty($sql)) {
			$this->querys['<#1>'] = new Query('<#1>', $sql, $datas, $databaseId, false);
		}
	}

	/**
	 * Add a query
	 *
	 * @param string $name name of this request
	 * @param  string|array $sql SQL query, see Query::addReq for syntax
	 * @param int|bool|string|array $datas you're datas, see Query::addDatas for syntax
	 * @return \Luri\BddI\QuerySet FluentInterface
	 */
	public function addQuery($name, $sql, $datas="") : QuerySet {
		$this->querys[$name] = new Query($name, $sql, $datas, $this->dbId, false);

		return $this;
	}

	/**
	 * Execute All query within a SQL transaction
	 *
	 * @return \Luri\BddI\QuerySet FluentInterface
	 */
	public function exe() : QuerySet{
		//Start a transaction
		$this->db->transactionStart();

		//Execute all query
		try {
			foreach ($this->querys as $v) {
				$v->exe();
			}
		} catch (Exception $ex) {
			//We have an error --> Rollback
			$this->db->transactionRollback();

			//On la relance
			throw $ex;
		}

		//If we here, we have no error, so commit
		$this->db->transactionCommit();

		return $this;
	}


	/***************
	 * INTERFACES  *
	 ***************/

	/**
	 * @return Query[]
	 */
	public function getIterator() {
		return new \ArrayIterator($this->querys);
	}

	public function count() {
		return count($this->querys);
	}

	/**
	 *
	 * @param int $offset
	 * @return bool
	 */
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->querys);
	}
	/**
	 *
	 * @param int $offset
	 * @return \Luri\BddI\Query
	 */
	public function offsetGet($offset) : Query {
		return $this->querys[$offset];
	}
	public function offsetSet($offset, $value) {
		if ($this->offsetExists($offset)) {
			//Change SQL
			$this->querys[$offset]->addReq($value);
		} else {
			//New SQL request
			$this->querys[$offset] = new Query($offset, $value, "", $this->dbId, false);
		}
	}
	public function offsetUnset($offset) {
		if ($this->offsetExists($offset)) {
			//Delete Request
			unset($this->querys[$offset]);
		} else {
			throw new \OutOfBoundsException("offset $offset not exist ");
		}
	}
}
?>