<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI;

use Luri\BddI\Common\SqlResponse;

/**
 * Represent a data to use in a sql request
 *
 */
class Query implements \ArrayAccess, \Countable, \IteratorAggregate {
	/**
	 * SQL request
	 * @var SqlRequestReal
	 */
	protected  $request = null;

	/**
	 * Response of SQL server
	 * @var SqlResponse
	 */
	protected $response = null;

	/**
	 * Id of this Query
	 *
	 * @var string
	 */
	protected $name = "";

	/**
	 * Reprensent the low level interface to SQL server
	 *
	 * @var Db
	 */
	protected $db = null;

	/**
	 * Name of DbId to use
	 *
	 * @var string
	 */
	protected $dbId = "";


	/**
	 * Create a new SQL Query
	 *
	 * All Param is optional
	 *
	 * @param string $name Name of your query. (it's for you)
	 * @param string|array $sql SQL query, see addReq for syntax
	 * @param int|bool|string|array $datas you're datas, see addDatas for syntax
	 * @param string $databaseId Database id where req is executed. If not provided, it's take ddefault database
	 * @param bool $exec Provide false if you don't want execute your request now
	 * @throws \InvalidArgumentException
	 * @return Query
	 */
	public static function factory($name="<NONAME>", $sql="", $datas = "", $databaseId="", $exec=true) {
		return new Query($name, $sql, $datas, $databaseId, $exec);
	}


	/**
	 * Create a new SQL Query
	 *
	 * All Param is optional
	 *
	 * @param string $name Name of your query. (it's for you)
	 * @param string|array $sql SQL query, see addReq for syntax
	 * @param int|bool|string|array $datas you're datas, see addDatas for syntax
	 * @param string $databaseId Database id where req is executed. If not provided, it's take default database
	 * @param bool $exec Provide false if you don't want execute your request now
	 * @throws \InvalidArgumentException
	 */
	public function __construct($name = "<NONAME>", $sql = "", $datas = "", $databaseId = "", $exec = true) {
		//Verify and add name
		if (!is_string($name)) {
			throw new \InvalidArgumentException("You must provide a correct name to your query");
		}
		$this->name = $name;

		//Db connexion
		if (empty($databaseId)) {
			$databaseId = DbFactory::getIdDatabaseDefault();
		}
		$this->db = DbFactory::getDbInstance($databaseId);
		$this->dbId = $databaseId;

		//Add SQL
		if (!empty($sql)) {
			$this->addReq($sql);
		}

		//Add Data
		if (!empty($datas)) {
			$this->addDatas($datas);
		}

		//Exec
		if(!empty($this->request) and $exec) {
			$this->exe();
		}
	}



	/**
	 * Add the SQL request
	 *
	 * Possible syntax :
	 * - a string : SQL request
	 * - a array :   [ 'SQL' => SQL request,
	 *   (optionnal)   'DATAS' => data associated (for syntax, see addDatas)
	 *
	 * @param string|array $sql SQL request with eventually datas
	 * @return Query return this object
	 */
	public function addReq($sql) {
		if ($this->request==null) {
			$this->request = new SqlRequestReal($sql);
		} else {
			$this->request->modifyReq($sql);
		}

		return $this;
	}

	/**
	 * Add Data(s) to request
	 *
	 * Possible syntax :
	 * - A data (int | string | bool) : this data is added successive to the existing data (anonymous parameter)
	 * - A array of datas (int | string | bool | array) : key represent the parameter (anonymous for int key, named (:XXX) for string key)
	 *
	 * Type of data is automatically deducted by the php type : string by default except for int and bool.
	 *
	 * bool is replaced by (int) 0 for false and (int) 1 for true.
	 *
	 * You can pass an array for use in a IN().
	 * Example :
	 * addReq("SELECT * FROM table WHERE id IN(:LIST)");
	 * addDatas([ ':LIST' => [1, 3, 5, 7] ]);
	 *
	 * For add one named parameter, you can use this syntax : ->addDatas($value, $named);
	 * example : $Query->addDatas(2, ':ID');
	 *
	 * @param mixed $datas
	 * @param string $named Name of named Parameter
	 *
	 * @return Query return this object
	 */
	public function addDatas($datas, $named="") {
		if (!empty($named)) {
			$datas = [$named => $datas];
		}
		$this->request->addDatas($datas);

		return $this;
	}

	/**
	 * Execute the Query
	 *
	 * @return Query return this object
	 */
	public function exe() {
		//Pour être sur...
		$this->db->changeDb($this->dbId);

		$this->response = $this->db->exe($this->request);

		return $this;
	}


	/*************************************
	 *                                   *
	 *************************************/


	/**
	 * Return a 2 dimension array with index is formed by $col
	 * Use this for a column who have more than 1 same value
	 *
	 * Example : ['Generation Z'] [0] = array (0 => 1, 'position' => 1, 1 => 'Allergic to the Sun', 'title' => 'Allergic to the Sun' )
	 *           ['Generation Z'] [1] = array (0 => 2, 'position' => 2, 1 => 'I Want You', 'title' => 'I Want You' )
	 *           ['Generation Z'] [2] = array (0 => 3, 'position' => 3, 1 => 'Thousand Years', 'title' => 'Thousand Years' )
	 *           ['143'] [0] =  array (0 => 1, 'position' => 1, 1 => 'Hopeful', 'title' => 'Hopeful' )
	 *           ['143'] [1] =  array (0 => 2, 'position' => 2, 1 => 'Breathe', 'title' => 'Breathe' )
	 *
	 * WARNING : return array can be big. Be sure you can't do diffrerently.
	 *
	 * @param int|string $col numerical position or name of field
	 * @return array
	 * @throws \BadFunctionCallException if query is not executed before
	 * @throws \OutOfBoundsException if field not exist in resut
	 */
	public function getDoubleArray($col=0) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}
		if (count($this->response)<=0) {
			//Pas de résultat, retourne un tableau vide
			return array();
		}

		//Return all field and use int (0, 1) or Name of field in param $col
		$this->response->setReturnIndex(SqlResponse::INDEXBOTH);
		$this->response->setColumns(SqlResponse::ALLCOLUMNS);

		//Column exist ?
		if (!array_key_exists($col, $this->response[0])) {
			//no
			throw new \OutOfBoundsException("[getDoubleArray] field $col not exist in result of query {$this->name} ");
		}

		$ret = [];
		foreach ($this->response as $v) {
			$ret[$v[$col]][] = $v;
		}

		return $ret;
	}

	/**
	 * Return a variable (first line of result)
	 *
	 * @param int|string $col numerical position or name of field
	 * @return int|string
	 * @throws \BadFunctionCallException if query is not executed before
	 * @throws \OutOfBoundsException if field not exist in resut
	 */
	public function getVar($col=0) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}
		if (count($this->response)<=0) {
			//Pas de résultat, retourne une variable vide
			return '';
		}

		//Return all field and use int (0, 1) or Name of field in param $col
		$this->response->setReturnIndex(SqlResponse::INDEXBOTH);
		$this->response->setColumns(SqlResponse::ALLCOLUMNS);

		//Verif
		if (!array_key_exists($col, $this->response[0])) {
			//Column not exist
			throw new \OutOfBoundsException("[getVar] field $col not exist in result of query {$this->name}");
		}

		return $this->response[0][$col];
	}

	/**
	 * Return an array wich countain only one field of the result
	 *
	 * @param int|string $col numerical position or name of field
	 * @return array
	 * @throws \BadFunctionCallException if query is not executed before
	 */
	public function getColumn($col=0) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		$this->response->setColumns([$col]);
		$ret = [];
		foreach ($this->response as $v) {
			$ret[] = current($v);
		}

		return $ret;
	}

	/**
	 * Return correllation between an ID and a value
	 *
	 * For example : SELECT id, name FROM table
	 * This function can return name corresponding to a specific id
	 *
	 * @param mixed $id value to search in column ID
	 * @param int|string $colId numerical position or name of field who is id
	 * @param int|string $colValue numerical position or name of field who want return
	 * @return int|string
	 * @throws \BadFunctionCallException if query is not executed before
	 */
	public function getCorrelation($id, $colId=0, $colValue=1) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		//Set
		$this->response->setColumns([$colValue]);

		//Return
		$t = $this->response->getLine($colId, $id);
		return current($t);
	}


	/**
	 * This function allow you to filtered column is returned by class (in foreach, [] or getLine() )
	 *
	 * @param string $columns (SqlResponse::ALLCOLUMNS for all the columns returned by SQL db)
	 */
	public function setColumns($columns= SqlResponse::ALLCOLUMNS) {
		$this->response->setColumns($columns);
	}

	/**
	 * This function allox you to choose type of index for the array who contain SQL data ONLY FOR foreach, [] or getLine()
	 *
	 * - SqlResponse::INDEXINT : Columns are returned into the array having an enumerated index.
	 *
	 * - SqlResponse::INDEXSTRING : Columns are returned into the array having the fieldname as the array index.  (by default)
	 *
	 * - SqlResponse::INDEXBOTH : Columns are returned into the array having both a numerical index and the fieldname as the associative index.  (this use more space in ram)
	 *
	 * @param int $indextype SqlResponse::INDEXINT | SqlResponse::INDEXSTRING (default) | SqlResponse::INDEXBOTH
	 */
	public function setReturnIndex($indextype = SqlResponse::INDEXSTRING) {
		$this->response->setReturnIndex($indextype);
	}

	/**
	 * Return the first line of result who countain $value in column $column
	 *
	 * Column can be set numerically (1rst column is 0) or by string (name of field)
	 *
	 * @param int|string $col
	 * @param mixed $value
	 * @return array
	 */
	public function getLine($col, $value) : array {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		return $this->response->getLine($col, $value);
	}

	/**
	 * Return 1 line of the result
	 *
	 * Also, you can use [] syntax : $query[$line]
	 *
	 * @param int $line
	 * @return array
	 */
	public function getLineNumber($line=0) : array {
		$t = $this->offsetGet($line);

		if (is_array($t)) {
			return $t;
		} else {
			//Probablement un résultat vide
			return array();
		}

	}

	public function __toString() {
		return "{$this->request}";
	}


	/*************************************
	 * INTERFACES (Redirect to response) *
	 *************************************/

	public function getIterator() {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		return $this->response;
	}

	/**
	 * For a SQL SELECT, SHOW, DESCRIBE or EXPLAIN, return the number of rows of the result
	 *
	 * For a SQL INSERT, UPDATE, REPLACE or DELETE, return  the number of affected rows.
	 *
	 * @return int
	 */
	public function count() {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		return count($this->response);
	}

	public function offsetExists($offset) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		return $this->response->offsetExists($offset);
	}
	public function offsetGet($offset) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		return $this->response->offsetGet($offset);
	}
	public function offsetSet($offset, $value) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		$this->response->offsetSet($offset, $value);
	}
	public function offsetUnset($offset) {
		if ($this->response == null) {
			//Query is not executed
			throw new \BadFunctionCallException("Error : Query {$this->name} is not executed");
		}

		$this->response->offsetUnset($offset);
	}
}
?>