<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
use PHPUnit\Framework\TestCase;
use Luri\BddI\{QuerySet, Query, DbFactory};
use \Luri\BddI\LowLevel\{LlMock, LlMockResponse};

class QuerySetTest extends TestCase {

	public function testTwoSimpleRequestSyntax1() {
		//
		// SYNTAX 1
		//
		$set = new QuerySet();
		$set->addQuery('<#1>', 'UPDATE table SET champ=\'N\' WHERE option=2');
		$set->addQuery('<#2>', 'UPDATE table2 SET champ=\'bob\' WHERE option=2');

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#1>']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#2>']);

		$set->exe();

		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$this->assertEquals('UPDATE table2 SET champ=\'bob\' WHERE option=2', array_pop($allrequest));
		$this->assertEquals('UPDATE table SET champ=\'N\' WHERE option=2', array_pop($allrequest));
	}

	public function testTwoSimpleRequestSyntax2() {
		//
		// SYNTAX 2
		//
		$set = new QuerySet();

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$set['#1'] = 'UPDATE table SET champ=\'N\' WHERE option=2';
		$set['#2'] = 'UPDATE table2 SET champ=\'bob\' WHERE option=2';

		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['#1']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['#2']);


		$set->exe();
		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$this->assertEquals('UPDATE table2 SET champ=\'bob\' WHERE option=2', array_pop($allrequest));
		$this->assertEquals('UPDATE table SET champ=\'N\' WHERE option=2', array_pop($allrequest));
	}

	public function testTwoSimpleRequestSyntax3() {
		//
		// SYNTAX 3
		//
		$set = new QuerySet('UPDATE table SET champ=\'N\' WHERE option=2');
		$set->addQuery('<#2>', 'UPDATE table2 SET champ=\'bob\' WHERE option=2');

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#1>']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#2>']);


		$set->exe();
		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$this->assertEquals('UPDATE table2 SET champ=\'bob\' WHERE option=2', array_pop($allrequest));
		$this->assertEquals('UPDATE table SET champ=\'N\' WHERE option=2', array_pop($allrequest));
	}

	public function testTwoSimpleRequestSyntax4() {
		//
		// SYNTAX 4
		//
		$set = QuerySet::factory('UPDATE table SET champ=\'N\' WHERE option=2')
			->addQuery('<#2>', 'UPDATE table2 SET champ=\'bob\' WHERE option=2')
			->exe();

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#1>']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#2>']);

		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$this->assertEquals('UPDATE table2 SET champ=\'bob\' WHERE option=2', array_pop($allrequest));
		$this->assertEquals('UPDATE table SET champ=\'N\' WHERE option=2', array_pop($allrequest));
	}

	public function testTwoRequestWithDatasSyntax2() {
		//
		// SYNTAX 2
		//
		$set = new QuerySet();

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$set['#1'] = 'UPDATE table SET champ=? WHERE option=?';
		//fwrite(STDERR, print_r("\nTOYOUTOYOU\n", TRUE));
		$set['#1']->addDatas('O');
		$set['#1']->addDatas(2);

		$set['#2'] = 'UPDATE table2 SET champ=? WHERE option=?';
		$set['#2']->addDatas('bob');
		$set['#2']->addDatas(2);

		//some Verify
		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['#1']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['#2']);


		$set->exe();
		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$req2 = array_pop($allrequest);
		$req1 = array_pop($allrequest);
		unset($allrequest);

		$this->assertEquals('UPDATE table SET champ=? WHERE option=?', $req1);
		$req_datas = [];
		foreach ($req1->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals(['O', 2], $req_datas);

		$this->assertEquals('UPDATE table2 SET champ=? WHERE option=?', $req2);
		$req_datas = [];
		foreach ($req2->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals(['bob', 2], $req_datas);

	}

	public function testTwoRequestWithDatasSyntax4() {
		//
		// SYNTAX 4
		//
		$set = QuerySet::factory([
				'SQL' => 'UPDATE table SET champ=? WHERE option=?',
				'DATAS' => ['O', 2]
			])
			->addQuery('<#2>', [
				'SQL' => 'UPDATE table2 SET champ=? WHERE option=?',
				'DATAS' => ['bob', 2]
			])
			->exe();

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		//some Verify
		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#1>']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['<#2>']);

		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$req2 = array_pop($allrequest);
		$req1 = array_pop($allrequest);
		unset($allrequest);

		$this->assertEquals('UPDATE table SET champ=? WHERE option=?', $req1);
		$req_datas = [];
		foreach ($req1->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals(['O', 2], $req_datas);

		$this->assertEquals('UPDATE table2 SET champ=? WHERE option=?', $req2);
		$req_datas = [];
		foreach ($req2->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals(['bob', 2], $req_datas);

	}

	public function testSpecificDatabase() {
		$set = new QuerySet("", "", 'default');

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$set['#1'] = 'UPDATE table SET champ=\'N\' WHERE option=2';
		$set['#2'] = 'UPDATE table2 SET champ=\'bob\' WHERE option=2';

		$this->assertEquals(2, count($set));
		$this->assertInstanceOf('\Luri\BddI\Query', $set['#1']);
		$this->assertInstanceOf('\Luri\BddI\Query', $set['#2']);
	}


	public function testRollback() {
		$this->expectException(\OutOfBoundsException::class);
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();
		$exception_rollback = new \OutOfBoundsException("Just for a rollback test");
		$mock->addException($exception_rollback);

		//Another other possible syntax to define set of Query
		$set = QuerySet::factory('UPDATE table SET champ=? WHERE option=?',
			['O', 2]
		)
		->addQuery('<#2>',
			'UPDATE table2 SET champ=? WHERE option=?',
			['bob', 2]
		)->exe();


	}

	public function testModifyQueryWithBrace() {
		$set = new QuerySet();
		$set['#1'] = 'UPDATE table SET champ=\'N\' WHERE option=2';
		$set['#2'] = 'UPDATE table2 SET champ=\'bob\' WHERE option=2';

		//On change la query #1
		$set['#1'] = 'Et puis finalement non';
		$set->exe();

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		// Verify Request executed.
		// We get All the request from the Mock Driver and pop the 2 last request
		// So Request #2 is pop in first.

		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$allrequest = $mock->getRequests();
		$req2 = array_pop($allrequest);
		$req1 = array_pop($allrequest);
		unset($allrequest);

		$this->assertEquals('Et puis finalement non', $req1);
		$this->assertEquals('UPDATE table2 SET champ=\'bob\' WHERE option=2', $req2);
	}

	public function testDeleteAQuery() {
		$set = new QuerySet();
		$set['#1'] = 'UPDATE table SET champ=\'N\' WHERE option=2';
		$set['#2'] = 'UPDATE table2 SET champ=\'bob\' WHERE option=2';

		$this->assertEquals(2, count($set));
		unset($set['#1']);
		$this->assertEquals(1, count($set));
	}

	public function testDeleteAQueryThatNotExist() {
		$this->expectException("\OutOfBoundsException");

		$set = new QuerySet();
		$set['#1'] = 'UPDATE table SET champ=\'N\' WHERE option=2';

		unset($set['#3']);
	}

	public function testForeach() {
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */

		//On enregistre les réponses qu'on veut voir retourner
		$mock = DbFactory::getDbInstance();
		$mock->addResponse(new LlMockResponse([
			[
				'query' => 1,
				'exemple' => 'Ronan Parke'
			],
			[
				'query' => 1,
				'exemple' => 'Greyson Chance'
			],
		]));
		$mock->addResponse(new LlMockResponse([
			[
				'query' => 2,
				'exemple' => 'Haute-Savoie'
			]
		]));


		$set = new QuerySet();
		$set['#1'] = 'SELECT * FROM table1';
		$set['#2'] = 'SELECT * FROM table2';
		$set->exe();

		// $set[x] return a Query object
		// In foreach($set as $v) { ... } $v is a Query object
		// But Netbeans autocomplete doesn't work well with ArrayAcess and IteratorAggregate interface
		//So, for use autocompletion, you must add this line :
		/* @var $set \Luri\BddI\Query[]  */

		$i=0;
		foreach ($set as $k => $v) {
			switch ($k) {
				case '#1':
					$this->assertEquals(2, count($v));
					$this->assertEquals(1, $v->getVar('query'));
					$this->assertEquals(['Ronan Parke', 'Greyson Chance'], $v->getColumn('exemple'));
					++$i;
					break;
				case '#2':
					$this->assertEquals(1, count($v));
					$this->assertEquals(2, $v->getVar('query'));
					$this->assertEquals(['Haute-Savoie'], $v->getColumn('exemple'));
					++$i;
					break;
				default :
					$this->fail('Too many datas!');
			}
		}
		$this->assertEquals(2, $i, 'Too few or too many datas!');
	}
}
?>