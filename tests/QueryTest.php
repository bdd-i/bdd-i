<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error\Error;
use Luri\BddI\{Query,DbFactory};
use Luri\BddI\LowLevel\{LlMock, LlMockResponse};
use Luri\BddI\Common\NotFoundException;


/**
 * This test use default response of LlMockResponse (Mock low level driver)
 */
class QueryTest extends TestCase {

	public function testSimpleRequest() {
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		//
		// Syntax 1 (immediate execute)
		//

		$datas = new Query(
			'<SimpleRequest#Syntax1>',
			'SELECT * FROM singer'
		);
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can just test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer', $mock->getLastRequest());

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 2 (delayed execute)
		//

		$datas = new Query('<SimpleRequest#Syntax2>');
		$datas->addReq('SELECT * FROM singer2');
		//Here the query is not executed, so data is not available
		$this->expectException('BadFunctionCallException');
		$this->assertNotEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);

		$datas->exe();
		//Now, we have the data
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can just test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer2', $mock->getLastRequest());

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 2bis (delayed execute)
		//

		$datas = new Query(
			'<SimpleRequest#Syntax2bis>',
			'SELECT * FROM singer3',
			"",
			DbFactory::getIdDatabaseDefault(),
			false
		);
		//Here the query is not executed, so data is not available
		$this->expectException('BadFunctionCallException');
		$this->assertNotEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);

		$datas->exe();
		//Now, we have the data
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can just test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer3', $mock->getLastRequest());

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 3 (fluent interface)
		//
		$datas = Query::factory('<SimpleRequest#Syntax3>')
			->addReq('SELECT * FROM singer4')
			->exe()
			->getLineNumber(0);
		//Here, we have the 1rst line of result in $datas
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas);
		//Or we can just test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer4', $mock->getLastRequest());
	}

	/**
	 * You can use Named Parameter even if php driver not support then
	 * (yes with mysqli, you can)
	 * See SqlRequestRealTest for example of different syntax of data parameter.
	 */
	public function testRequestWithOneData() {
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		//
		// Syntax 1 (immediate execute)
		//

		$datas = new Query(
			'<OneData#Syntax1>',
			'SELECT * FROM singer WHERE param = ?',
			2
		);
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer WHERE param = ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2], $req_datas);

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 2 (delayed execute)
		//

		$datas = new Query('<OneData#Syntax2>');
		$datas->addReq('SELECT * FROM singer2 WHERE param = ?');
		$datas->addDatas(2);
		//Here the query is not executed, so data is not available
		$this->expectException('BadFunctionCallException');
		$this->assertNotEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);

		$datas->exe();
		//Now, we have the data
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer2 WHERE param = ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2], $req_datas);

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 2bis (delayed execute)
		//

		$datas = new Query(
			'<OneData#Syntax2bis>',
			'SELECT * FROM singer2bis WHERE param = ?',
			2,
			DbFactory::getIdDatabaseDefault(),
			false
		);
		//Here the query is not executed, so data is not available
		$this->expectException('BadFunctionCallException');
		$this->assertNotEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);

		$datas->exe();
		//Now, we have the data
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer2bis WHERE param = ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2], $req_datas);

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 3 (fluent interface)
		//
		$datas = Query::factory('<OneData#Syntax3>')
			->addReq('SELECT * FROM singer3 WHERE param = ?')
			->addDatas(2)
			->exe() //execute the request
			->getLineNumber(0); //get result (1rst line of default mock response)

		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas);

		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer3 WHERE param = ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2], $req_datas);
	}

	public function testRequestWithMultipleDatas() {
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		//
		// Syntax 1 (immediate execute)
		//

		$datas = new Query(
			'<MultipleDatas#Syntax1>',
			'SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE :NATIONALITY',
			[
				':NATIONALITY' => 'British',
				':ID' => 2
			]
		);
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2, 'British'], $req_datas);

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 2 (delayed execute)
		//

		$datas = new Query('<MultipleDatas#Syntax2>');
		$datas->addReq('SELECT * FROM singer2 WHERE idsinger=:ID OR nationality LIKE :NATIONALITY');
		//If you can't use second parameter for addDatas, you must be passed data in same order than request
		$datas->addDatas('British', ':NATIONALITY');
		$datas->addDatas(2, ':ID');
		//Here the query is not executed, so data is not available
		$this->expectException('BadFunctionCallException');
		$this->assertNotEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);

		$datas->exe();
		//Now, we have the data
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer2 WHERE idsinger=? OR nationality LIKE ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2, 'British'], $req_datas);

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 2bis (delayed execute)
		//

		$datas = new Query(
			'<MultipleDatas#Syntax2bis>',
			'SELECT * FROM singer2bis WHERE idsinger=:ID OR nationality LIKE :NATIONALITY',
			[
				':NATIONALITY' => 'British',
				':ID' => 2
			],
			DbFactory::getIdDatabaseDefault(),
			false
		);
		//Here the query is not executed, so data is not available
		$this->expectException('BadFunctionCallException');
		$this->assertNotEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);

		$datas->exe();
		//Now, we have the data
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer2bis WHERE idsinger=? OR nationality LIKE ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2, 'British'], $req_datas);

		//Must do this to sure request is free
		unset($datas);


		//
		// Syntax 3 (fluent interface)
		//
		$datas = Query::factory('<MultipleDatas#Syntax3>')
			->addReq('SELECT * FROM singer3 WHERE idsinger=:ID OR nationality LIKE ?')
			->addDatas([
				'British', //Yes it's working
				':ID' => 2
			])
			->exe() //execute the request
			->getLineNumber(0); //get result (1rst line of default mock response)

		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas);

		//Or we can test the request passed to Mock
		$this->assertEquals('SELECT * FROM singer3 WHERE idsinger=? OR nationality LIKE ?', $mock->getLastRequest());
		$req_datas = [];
		foreach ($mock->getLastRequest()->getDatas() as $k=>$v) {
			$req_datas[$k] = $v;
		}
		$this->assertEquals([2, 'British'], $req_datas);
	}

	public function testRequestInSpecificDatabase() {
		$datas = $datas = new Query(
			'<SpecificDatabase>',
			'SELECT * FROM singer',
			"",
			'default' //use here id configured in DbConfig
		);
		//If we have the first line of default mock response : Execute is correct.
		$this->assertEquals(['idsinger' => 1, 'name' => 'Bars and Melody', 'nationality' => 'British'], $datas[0]);
		//Must do this to sure request is free
		unset($datas);
	}

	public function testGetResponseDoubleArray() {
		// Here, We use defaut response of Mock. But You can set reponse you want have.
		// Example code below :
		// /* @var $mock \Luri\BddI\LowLevel\LlMock  */
		// $mock = DbFactory::getDbInstance();
		//
		// $response = new LlMockResponse(['take', 'your', 'response']);
		// $mock->setDefaultResponse($response);

		$datas = Query::factory('<testGetResponseDoubleArray>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getDoubleArray('nationality');

		$expected = $this->values = [
			'British' => [
				[
					'idsinger' => 1,
					'name' => 'Bars and Melody',
					'nationality' => 'British',
					1,
					'Bars and Melody',
					'British',
				],
				[
					'idsinger' => 5,
					'name' => 'Ronan Parke',
					'nationality' => 'British',
					5,
					'Ronan Parke',
					'British',
				]
			],
			'French' => [[
				'idsinger' => 2,
				'name' => 'Eddy De Pretto',
				'nationality' => 'French',
				2,
				'Eddy De Pretto',
				'French',
			]],
			'U.S.A.' => [[
				'idsinger' => 3,
				'name' => 'Greyson Chance',
				'nationality' => 'U.S.A.',
				3,
				'Greyson Chance',
				'U.S.A.',
			]],
			'Australian' => [[
				'idsinger' => 4,
				'name' => 'Troye Sivan',
				'nationality' => 'Australian',
				4,
				'Troye Sivan',
				'Australian',
			]],
		];
		$this->assertEquals($expected, $datas);

		$datas = Query::factory('<testGetResponseDoubleArray2>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getDoubleArray(2);
		$this->assertEquals($expected, $datas);
	}
	public function testGetResponseDoubleArrayWithInvalidParameter() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testGetResponseDoubleArrayWithInvalidParameter>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getDoubleArray('prenom');
	}
	public function testGetResponseDoubleArrayButQueryNotExecuted() {
		$this->expectException('BadFunctionCallException');

		$datas = Query::factory('<testGetResponseDoubleArrayButQueryNotExecuted>')
			->addReq('SELECT * FROM singer')
			->getDoubleArray('nationality');
	}

	public function testGetResponseVar() {
		$datas = Query::factory('<testGetResponseVar>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getVar('name'); //numerical position work. You can also pass 1 (see below)
		$this->assertEquals('Bars and Melody', $datas);

		$datas = Query::factory('<testGetResponseVar>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getVar(1);
		$this->assertEquals('Bars and Melody', $datas);
	}
	public function testGetResponseVarWithInvalidParameter() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testGetResponseVarWithInvalidParameter>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getVar(100);
	}
	public function testGetResponseVarButQueryNotExecuted() {
		$this->expectException('BadFunctionCallException');

		$datas = Query::factory('<testGetResponseVarButQueryNotExecuted>')
			->addReq('SELECT * FROM singer')
			->getVar(1);
	}

	public function testGetResponseColumn() {
		$datas = Query::factory('<testGetResponseColumn>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getColumn('name'); //you can also pass numerical position (here : 1)

		$expected = [
			'Bars and Melody',
			'Eddy De Pretto',
			'Greyson Chance',
			'Troye Sivan',
			'Ronan Parke'
		];
		$this->assertEquals($expected, $datas);

		$datas = Query::factory('<testGetResponseColumn2>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getColumn(1);
		$this->assertEquals($expected, $datas);
	}
	public function testGetResponseColumnWithInvalidParameter() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testGetResponseColumnWithInvalidParameter>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getColumn(100);
	}
	public function testGetResponseColumnButQueryNotExecuted() {
		$this->expectException('BadFunctionCallException');

		$datas = Query::factory('<testGetResponseColumnButQueryNotExecuted>')
			->addReq('SELECT * FROM singer')
			->getColumn('name');
	}

	public function testGetResponseCorrelation() {
		$datas = Query::factory('<testGetResponseCorrelation>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getCorrelation(5, 'idsinger', 'nationality');
		$this->assertEquals('British', $datas);

		$datas = Query::factory('<testGetResponseCorrelation2>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getCorrelation(5, 0, 2);
		$this->assertEquals('British', $datas);
	}
	public function testGetResponseCorrelationWithInvalidParameter1() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testGetResponseCorrelationWithInvalidParameter1>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getCorrelation(5, 'notexist', 'nationality');
	}
	public function testGetResponseCorrelationWithInvalidParameter2() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testGetResponseCorrelationWithInvalidParameter2>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getCorrelation(5, 'idsinger', 'notexist');
	}
	public function testGetResponseCorrelationButValueNotFound() {
		$this->expectException(\Luri\BddI\Common\NotFoundException::class);

		$datas = Query::factory('<testGetResponseCorrelationButValueNotFound>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getCorrelation(77, 'idsinger', 'nationality');
	}
	public function testGetResponseCorrelationButQueryNotExecuted() {
		$this->expectException('BadFunctionCallException');

		$datas = Query::factory('<testGetResponseCorrelationButQueryNotExecuted>')
			->addReq('SELECT * FROM singer')
			->getCorrelation(5, 'idsinger', 'nationality');
	}


	public function testGetResponseBrace() {
		$datas = new Query('<testGetResponseBrace>', 'SELECT * FROM singer');

		$expected = [
			'idsinger' => 5,
			'name' => 'Ronan Parke',
			'nationality' => 'British',
		];
		$this->assertEquals($expected, $datas[4]); //1rst Line of result is 0
	}
	public function testGetResponseBraceButQueryNotExecuted() {
		$this->expectException('BadFunctionCallException');

		$query = new Query('<testGetResponseBraceButQueryNotExecuted>');
		$query->addReq('SELECT * FROM singer');
		$data = $query[4];

	}

	public function testGetResponseForeach() {
		$datas = new Query('<testGetResponseForeach>', 'SELECT * FROM singer');

		foreach ($datas as $k => $v) {
			switch ($k) {
				case '0':
					$expected = [
						'idsinger' => 1,
						'name' => 'Bars and Melody',
						'nationality' => 'British',
					];
					$this->assertEquals($expected, $v);
					break;

				case '1':
					$expected = [
						'idsinger' => 2,
						'name' => 'Eddy De Pretto',
						'nationality' => 'French',
					];
					$this->assertEquals($expected, $v);
					break;

				case '2':
					$expected = [
						'idsinger' => 3,
						'name' => 'Greyson Chance',
						'nationality' => 'U.S.A.',
					];
					$this->assertEquals($expected, $v);
					break;

				case '3':
					$expected = [
						'idsinger' => 4,
						'name' => 'Troye Sivan',
						'nationality' => 'Australian',
					];
					$this->assertEquals($expected, $v);
					break;

				case '4':
					$expected = [
						'idsinger' => 5,
						'name' => 'Ronan Parke',
						'nationality' => 'British',
					];
					$this->assertEquals($expected, $v);
					break;

				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(4, $k, 'Too few or too many datas!');
	}
	public function testGetResponseForeachButQueryNotExecuted(){
		$this->expectException('BadFunctionCallException');

		$query = new Query('<testGetResponseBraceButQueryNotExecuted>');
		$query->addReq('SELECT * FROM singer');

		foreach ($query as $k => $v) {
			//must not work
			$this->assertFalse(true, 'This must no do here');
		}
	}

	public function testSearchOneLineInResult() {
		$datas = Query::factory('<testSearchOneLineInResult>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getLine('idsinger', 3);

		$expected = [
			'idsinger' => 3,
			'name' => 'Greyson Chance',
			'nationality' => 'U.S.A.'
		];
		$this->assertEquals($expected, $datas);

		$datas = Query::factory('<testSearchOneLineInResult>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getLine(0, 3);
		$this->assertEquals($expected, $datas);
	}
	public function testSearchOneLineInResultWithInvalidParameter1() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testSearchOneLineInResultWithInvalidParameter1>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getLine('id', 3);
	}

	public function testSearchOneLineInResultWithInvalidParameter2() {
		$this->expectException('OutOfBoundsException');

		$datas = Query::factory('<testSearchOneLineInResultWithInvalidParameter2>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getLine(266, 3);
	}

	public function testSearchOneLineInResultButvalueNotFound() {
		$this->expectException(NotFoundException::class);

		$datas = Query::factory('<testSearchOneLineInResultButvalueNotFound>')
			->addReq('SELECT * FROM singer')
			->exe()
			->getLine('idsinger', 300);
	}

	public function testSearchOneLineInResultButQueryNotExecuted() {
		$this->expectException('BadFunctionCallException');

		$datas = Query::factory('<testSearchOneLineInResultButvalueNotFound>')
			->addReq('SELECT * FROM singer')
			->getLine('idsinger', 3);
	}

	public function testFilteredColumnInResults() {
		$datas = new Query('<testFilteredColumnInResults>', 'SELECT * FROM singer');
		$datas->setColumns(['name', 'nationality']);

		foreach ($datas as $k => $v) {
			switch ($k) {
				case '0':
					$expected = [
						'name' => 'Bars and Melody',
						'nationality' => 'British',
					];
					$this->assertEquals($expected, $v);
					break;

				case '1':
					$expected = [
						'name' => 'Eddy De Pretto',
						'nationality' => 'French',
					];
					$this->assertEquals($expected, $v);
					break;

				case '2':
					$expected = [
						'name' => 'Greyson Chance',
						'nationality' => 'U.S.A.',
					];
					$this->assertEquals($expected, $v);
					break;

				case '3':
					$expected = [
						'name' => 'Troye Sivan',
						'nationality' => 'Australian',
					];
					$this->assertEquals($expected, $v);
					break;

				case '4':
					$expected = [
						'name' => 'Ronan Parke',
						'nationality' => 'British',
					];
					$this->assertEquals($expected, $v);
					break;

				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(4, $k, 'Too few or too many datas!');
	}

	public function testResultWithIntIndexFofColumn() {
		$datas = new Query('<testResultWithIntIndexFofColumn>', 'SELECT * FROM singer');
		$datas->setReturnIndex(Luri\BddI\Common\SqlResponse::INDEXINT);

		foreach ($datas as $k => $v) {
			switch ($k) {
				case '0':
					$expected = [
						1,
						'Bars and Melody',
						'British',
					];
					$this->assertEquals($expected, $v);
					break;

				case '1':
					$expected = [
						2,
						'Eddy De Pretto',
						'French',
					];
					$this->assertEquals($expected, $v);
					break;

				case '2':
					$expected = [
						3,
						'Greyson Chance',
						'U.S.A.',
					];
					$this->assertEquals($expected, $v);
					break;

				case '3':
					$expected = [
						4,
						'Troye Sivan',
						'Australian',
					];
					$this->assertEquals($expected, $v);
					break;

				case '4':
					$expected = [
						5,
						'Ronan Parke',
						'British',
					];
					$this->assertEquals($expected, $v);
					break;

				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(4, $k, 'Too few or too many datas!');
	}

	public function testResultWithIntAndAssociativeIndexForColumn() {
		$datas = new Query('<testResultWithIntAndAssociativeIndexForColumn>', 'SELECT * FROM singer');
		$datas->setReturnIndex(Luri\BddI\Common\SqlResponse::INDEXBOTH);

		foreach ($datas as $k => $v) {
			switch ($k) {
				case '0':
					$expected = [
						'idsinger' => 1,
						'name' => 'Bars and Melody',
						'nationality' => 'British',
						1,
						'Bars and Melody',
						'British',
					];
					$this->assertEquals($expected, $v);
					break;

				case '1':
					$expected = [
						'idsinger' => 2,
						'name' => 'Eddy De Pretto',
						'nationality' => 'French',
						2,
						'Eddy De Pretto',
						'French',
					];
					$this->assertEquals($expected, $v);
					break;

				case '2':
					$expected = [
						'idsinger' => 3,
						'name' => 'Greyson Chance',
						'nationality' => 'U.S.A.',
						3,
						'Greyson Chance',
						'U.S.A.',
					];
					$this->assertEquals($expected, $v);
					break;

				case '3':
					$expected = [
						'idsinger' => 4,
						'name' => 'Troye Sivan',
						'nationality' => 'Australian',
						4,
						'Troye Sivan',
						'Australian',
					];
					$this->assertEquals($expected, $v);
					break;

				case '4':
					$expected = [
						'idsinger' => 5,
						'name' => 'Ronan Parke',
						'nationality' => 'British',
						5,
						'Ronan Parke',
						'British',
					];
					$this->assertEquals($expected, $v);
					break;

				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(4, $k, 'Too few or too many datas!');
	}

	public function testCountNumberOfLineInResponse() {
		$datas = new Query('<testCountNumberOfLineInResponse>', 'SELECT * FROM singer');
		$this->assertEquals(5, count($datas));
	}

	public function testGetStringRequest() {
		$datas = new Query('<testResultWithIntAndAssociativeIndexForColumn>', 'SELECT * FROM singer');
		$this->assertEquals('SELECT * FROM singer', "$datas");
	}

	public function testGetLineNumber() {
		$datas = new Query('<testGetResponseBrace>', 'SELECT * FROM singer');

		$expected = [
			'idsinger' => 5,
			'name' => 'Ronan Parke',
			'nationality' => 'British',
		];
		$this->assertEquals($expected, $datas->getLineNumber(4)); //1rst Line of result is 0
	}

	public function testGetLineNumberButLineNotExist() {
		$datas = new Query('<testGetLineNumberButLineNotExist>', 'SELECT * FROM singer');

		$this->expectException(Error::class);
		$res = $datas->getLineNumber(99); //This line not exist
	}

	public function testGetLineNumberButEmptyResponse() {
		//Define Empty Response
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();

		$response = new LlMockResponse([]);
		$mock->setDefaultResponse($response);

		//Query and Text
		$datas = new Query('<testGetLineNumberButNoResponse>', 'SELECT * FROM singer');

		$res = $datas->getLineNumber();
		$this->assertIsArray($res);
		$this->assertEmpty($res);
	}

	public function testArrayAccessWithOffsetError() {
		$datas = new Query('<testGetLineNumberButLineNotExist>', 'SELECT * FROM singer');

		$this->expectException(Error::class);
		$res = $datas[99]; //This line not exist
	}
}
?>