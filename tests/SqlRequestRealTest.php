<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
use PHPUnit\Framework\TestCase;
use Luri\BddI\SqlRequestReal;

class SqlRequestTest extends TestCase {

	/**
	 * Test opérateur new
	 */
	public function testCreateRequestWithStringSql() {
		$req = new SqlRequestReal("SELECT * FROM singer");

		$this->assertEquals(false, $req->isData());
		$this->assertEquals("SELECT * FROM singer", $req);
	}

	public function testCreateRequestWithSqlAndDatas() {
		$req = new SqlRequestReal([
			'SQL' => 'SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE ?',
			'DATAS' => [
				':ID' => 2,
				'British'
			]
		]);

		$this->assertEquals(true, $req->isData());
		$this->assertEquals("SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ?", $req);
		foreach ($req->getDatas() as $k => $data) {
			switch ($k) {
				case 0:
					$this->assertEquals(2, $data);
					break;
				case 1:
					$this->assertEquals('British', $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(1, $k, 'Too few or too many datas!');


		//Retest with datas inversed
		unset($req);
		$req = new SqlRequestReal([
			'SQL' => 'SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE ?',
			'DATAS' => [
				'British',
				':ID' => 2
			]
		]);

		$this->assertEquals(true, $req->isData());
		$this->assertEquals("SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ?", $req);
		foreach ($req->getDatas() as $k=>$data) {
			switch ($k) {
				case 0:
					$this->assertEquals(2, $data);
					break;
				case 1:
					$this->assertEquals('British', $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(1, $k, 'Too few or too many datas!');
	}

	public function testModifyRequestWithStringSql() {
		$req = new SqlRequestReal("");

		$req->modifyReq("SELECT * FROM singer");
		$this->assertEquals(false, $req->isData());
		$this->assertEquals("SELECT * FROM singer", $req);
	}

	public function testModifyRequestWithSqlAndDatas() {
		$req = new SqlRequestReal("");

		$req->modifyReq([
			'SQL' => 'SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE ?',
			'DATAS' => [
				':ID' => 2,
				'British'
			]
		]);

		$this->assertEquals(true, $req->isData());
		$this->assertEquals("SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ?", $req);
		foreach ($req->getDatas() as $k=>$data) {
			switch ($k) {
				case 0:
					$this->assertEquals(2, $data);
					break;
				case 1:
					$this->assertEquals('British', $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(1, $k, 'Too few or too many datas!');
	}

	public function testAddSimpleData() {
		$req = new SqlRequestReal('SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE ? OR  isvalid = ?');
		//With use addDatas with not an array in parameter, you must add the parameter in same order wich use in the query
		//anonymous string parameter
		$req->addDatas(2);
		//in marqued parameter
		$req->addDatas('British');
		//bool anonymous parameter
		$req->addDatas(true);

		//verify
		$this->assertEquals(true, $req->isData());
		$this->assertEquals("SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ? OR  isvalid = ?", $req);
		foreach ($req->getDatas() as $k=>$data) {
			switch ($k) {
				case 0:
					$this->assertEquals(2, $data);
					break;
				case 1:
					$this->assertEquals('British', $data);
					break;
				case 2:
					$this->assertEquals(1, $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(2, $k, 'Too few or too many datas!');


		// Other Method

		unset($req);
		$req = new SqlRequestReal("");

		$req->modifyReq('SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE ? OR  isvalid = ?')
			->addDatas(2)
			->addDatas('British')
			->addDatas(true);

		//verify
		$this->assertEquals(true, $req->isData());
		$this->assertEquals("SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ? OR  isvalid = ?", $req);
		foreach ($req->getDatas() as $k=>$data) {
			switch ($k) {
				case 0:
					$this->assertEquals(2, $data);
					break;
				case 1:
					$this->assertEquals('British', $data);
					break;
				case 2:
					$this->assertEquals(1, $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(2, $k, 'Too few or too many datas!');

	}

	public function testAddMoreDataThanExistInQuery() {
		$this->expectException('OverflowException');

		$req = new SqlRequestReal('SELECT * FROM singer WHERE idsinger=?');
		$req->addDatas(2)
			->addDatas('British');
	}

	public function testAddMarquedParameterThatNotExist() {
		$this->expectException('OutOfBoundsException');

		$req = new SqlRequestReal('SELECT * FROM singer WHERE idsinger=:ID');
		$req->addDatas([':ID' => 2])
			->addDatas([':NATIONALITY' => 'British']);
	}

	public function testAdd3DatasInOnce() {
			$req = new SqlRequestReal('SELECT * FROM singer WHERE idsinger=:ID OR nationality LIKE ? OR  isvalid = ?');
		//anonymous string parameter
		$req->addDatas([
			':ID' => 2,  //int marqued parameter
			'British', //string anonymous parameter
			true //bool anonymous parameter
		]);

		//verify
		$this->assertEquals(true, $req->isData());
		$this->assertEquals("SELECT * FROM singer WHERE idsinger=? OR nationality LIKE ? OR  isvalid = ?", $req);
		foreach ($req->getDatas() as $k=>$data) {
			switch ($k) {
				case 0:
					$this->assertEquals(2, $data);
					break;
				case 1:
					$this->assertEquals('British', $data);
					break;
				case 2:
					$this->assertEquals(1, $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(2, $k, 'Too few or too many datas!');
	}

	public function testAddArrayDataFoxUseWithIN() {
		$req = new SqlRequestReal('');
		$req->modifyReq('SELECT * FROM singer WHERE idsinger IN (:LIST_ID)')
			->addDatas([
				':LIST_ID' => [1, 3, 5]
			]);

		$this->assertEquals(true, $req->isData());
		$this->assertEquals('SELECT * FROM singer WHERE idsinger IN (?,?,?)', "$req");
		foreach ($req->getDatas() as $k=>$data) {
			switch ($k) {
				case 0:
					$this->assertEquals(1, $data);
					break;
				case 1:
					$this->assertEquals(3, $data);
					break;
				case 2:
					$this->assertEquals(5, $data);
					break;
				default :
					$this->assertFalse(true, 'Too many datas!');
			}
		}
		$this->assertEquals(2, $k, 'Too few or too many datas!');
	}

	public function testSetNamedParameter() {
		$req = new SqlRequestReal("SELECT * FROM singer WHERE idsinger = :ID");
		$req->setNamedParameter(true);

		$this->assertEquals("SELECT * FROM singer WHERE idsinger = :ID", $req);
	}



}
?>