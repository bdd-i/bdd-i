<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
use PHPUnit\Framework\TestCase;
use Luri\BddI\DbFactory;
use Luri\BddI\LowLevel\LlMock;


class DbFactoryTest extends TestCase {

	public function testGetDefaultDatabase() {
		$this->assertEquals('default', DbFactory::getIdDatabaseDefault());
	}

	public function testGetDatabase() {
		$this->assertInstanceOf('\Luri\BddI\LowLevel\LlMock', DbFactory::getDbInstance('default'));
	}

	public function testGetDatabaseDefault() {
		$this->assertInstanceOf('\Luri\BddI\LowLevel\LlMock', DbFactory::getDbInstance());
	}

	public function testGetFalseDatabase() {
		$this->expectException('OutOfBoundsException');
		$this->assertNotInstanceOf('\Luri\BddI\LowLevel\LlMock', DbFactory::getDbInstance('error'));
	}

	public function testVerifyConnectionParameter() {
		/* @var $mock \Luri\BddI\LowLevel\LlMock  */
		$mock = DbFactory::getDbInstance();
		$connection = $mock->getConnectionParameter();

		$this->assertEquals('127.0.0.1', $connection['server']);
		$this->assertEquals('root', $connection['login']);
		$this->assertEquals('', $connection['password']);
		$this->assertEquals('latin1', $connection['charset']);
	}
}
?>